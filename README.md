# Speed-up patch for EPW

There have been some inefficient calls to BLAS ZAXPY in wan2bloch.f90 of EPW, for use_ws = .true. case at lease since version 6.4.

The patches provided here are meant to execute efficient calls to ZAXPY without copying array slices, see discussions here: https://forum.epw-code.org/viewtopic.php?f=3&t=1618

The best speed-up achieved can be as large as 200%. Test with care before significant calculations.

I also fixed a bug for etf_mem == 0 case, as discussed in the Issue I raised: https://gitlab.com/QEF/q-e/-/issues/492 . This bug has been fixed in qe-7.1rc / EPWv5.5rc.

If patches are not provided for your prefered version, please let me know or modify the code in a similar manner.

## Usage

Simply replace the wan2bloch.f90 in EPW/src by the one provided here for corresponding QE version.
